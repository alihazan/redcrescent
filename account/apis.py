from rest_framework import viewsets, status, permissions
from .serializers import (CustomTokenObtainPairSerializer, )
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from rest_framework_simplejwt.tokens import RefreshToken


class ObtainTokenPairApi(TokenObtainPairView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = CustomTokenObtainPairSerializer


class LogoutAndBlacklistRefreshTokenApi(APIView):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = ()

    def post(self, request):
        try:
            refresh_token = request.data["refresh"]
            token = RefreshToken(refresh_token)
            token.blacklist()
            return Response({"status":"Token Blacklisted"},status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"status":"Error"},status=status.HTTP_400_BAD_REQUEST)

