from django.urls import path
from django.conf.urls import include
from rest_framework_simplejwt import views as jwt_views
from .apis import (ObtainTokenPairApi,
     LogoutAndBlacklistRefreshTokenApi )  
 


urlpatterns = [
    path('password-reset/', include(
        'django_rest_passwordreset.urls', namespace='password_reset')),
    path('token/obtain/', ObtainTokenPairApi.as_view(), name='token_create'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('blacklist/', LogoutAndBlacklistRefreshTokenApi.as_view(), name='blacklist'),
]
