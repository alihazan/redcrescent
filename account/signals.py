
from django.dispatch import receiver
from django_rest_passwordreset.signals import reset_password_token_created
from django.core.mail import send_mail
from rest_framework import status
from rest_framework.response import Response


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    """
    Handles password reset tokens
    When a token is created, an e-mail needs to be sent to the user
    :param sender: View Class that sent the signal
    :param instance: View Instance that sent the signal
    :param reset_password_token: Token Model Object
    :param args:
    :param kwargs:
    :return:
    """
        # send an e-mail to the user
    subject='Password Reset'
    body = "Hi, %s, please use the token - \" %s \" for resetting your password." % (
        reset_password_token.user.first_name, reset_password_token.key) 
    from_email='noreplay@test.ae'

    to=[reset_password_token.user.email]
  
    try:
        send_mail(
            subject,
            body,
            from_email,
            to,
            fail_silently=False,
        )
      
    except Exception as e:
        return Response({'error':e},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
  

    