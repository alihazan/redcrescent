from django.urls import path,re_path
from django.conf.urls import include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.conf import settings

schema_view = get_schema_view(
   openapi.Info(
      title="Redcrescent crowdfunding - API",
      default_version='v1',
      description="Redcrescent crowdfunding application is a platform, in which users can make donation and make fundraisers.",
      terms_of_service="https://mvp-apps.ae/",
      contact=openapi.Contact(email="info@mvp-apps.com"),
      license=openapi.License(name="All right reserved by mvp-apps.ae"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

urlpatterns = [
   re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
   re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
   re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

if settings.DEBUG:
   from django.contrib.staticfiles.urls import staticfiles_urlpatterns
   from django.conf.urls.static import static
   urlpatterns += [ re_path(r'^silk/', include('silk.urls', namespace='silk')),]
   urlpatterns += staticfiles_urlpatterns()
   urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
   )
    

   