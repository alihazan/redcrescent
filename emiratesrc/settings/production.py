from .base import *

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

# Here we changing our db to local hosted postgres db so we can avoid dockerized db
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'emiratesrc',
        'USER': 'root',
        'PASSWORD': 'Root1234',
        'HOST': 'host-id',
        'PORT': "5432"
    }
}


DEBUG = False

ALLOWED_HOSTS = []


