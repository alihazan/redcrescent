from .base import *

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-jug+#4tu0j*=d(-cnwlz$03ut3*pc4ek-%loc^*n19(7#r3oq6'

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

# Here we changing our db to local hosted postgres db so we can avoid dockerized db
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'emiratesrc',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT': "8889"
    }
}

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']
CORS_ORIGIN_WHITELIST = (
    'http://localhost:8080',
)

CORS_ORIGIN_ALLOW_ALL = True

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# https://silk.readthedocs.io/
MIDDLEWARE += [
    'silk.middleware.SilkyMiddleware',
]

INSTALLED_APPS += [
    'silk',
]
