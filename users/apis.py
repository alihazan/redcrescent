from .models import User
from .serializers import UserSerializer
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from .permissions import AccountPermission

class UserViewSet(viewsets.ModelViewSet):
    """
    For managing user in application
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [AccountPermission,]