from .models import User
from rest_framework import serializers
from django_countries.serializer_fields import CountryField
from django.contrib.auth.hashers import make_password

class UserSerializer(serializers.ModelSerializer):
    country = CountryField()

    @staticmethod
    def validate_password(password: str) -> str:
        return make_password(password)

    class Meta:
        model = User
        exclude = ['last_login','is_superuser','is_active','is_staff','groups','user_permissions']
        extra_kwargs = {
            'password': {'write_only': True}
        }

        

