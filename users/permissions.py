from rest_framework.permissions import BasePermission

SAFE_METHODS = ['GET', 'HEAD', 'OPTIONS']

class AccountPermission(BasePermission):
    """
    User Account Permission defined here
    """
    
    def has_object_permission(self, request, view, obj):

        if request.method in SAFE_METHODS:
            return True
       

        return obj == request.user