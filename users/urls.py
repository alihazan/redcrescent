from users.apis import UserViewSet
from rest_framework.routers import DefaultRouter
from django.urls import path,include,re_path

router = DefaultRouter()
router.register(r'users', UserViewSet, basename='user')

urlpatterns = [
    path('', include(router.urls)),
]