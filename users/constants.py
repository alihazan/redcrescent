
LANGUAGE_CODE = (
    ('en_US','English (US)'),
    ('en_GB','English (UK)'),
    ('es_ES','Español'),
    ('de_DE','Deutsch'),
    ('fr_FR','French'),
    ('it_IT','Italiano'),
    ('nl_NL','Nederlands'),
    ('pt_BR','Português'),
)