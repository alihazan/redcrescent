from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from django_countries.fields import CountryField
from .constants import LANGUAGE_CODE
from .managers import UserManager



class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=30, blank=True,null=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)
    is_staff = models.BooleanField(_('staff status'),default=False)
    avatar = models.ImageField(_('avatar'),upload_to='multi_media/avatars/', null=True, blank=True)
    phone_number = models.CharField(_('phone number'),max_length=15,null=True,blank=True)
    language = models.CharField(_('language'),max_length = 5,choices=LANGUAGE_CODE,default='en_GB')
    country = CountryField(_('select country'),null=True,blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'


    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def __str__(self):
        return self.email
